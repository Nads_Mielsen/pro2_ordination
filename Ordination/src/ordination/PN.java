package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> dosisGivetDatoer;

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
		this.dosisGivetDatoer = new ArrayList<LocalDate>();
	}

	public ArrayList<LocalDate> getDosisGivetDatoer() {
		return new ArrayList<LocalDate>(dosisGivetDatoer);
	}

	public void setAntalEnheder(double antalEnheder) {
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean valid = false;

		if (givesDen.compareTo(super.getStartDen()) >= 0 && givesDen.compareTo(super.getSlutDen()) <= 0) {
			valid = true;
		}

		if (valid) {
			dosisGivetDatoer.add(givesDen);
		}

		return valid;
	}

	// Tag mængden delt med antal dage mellem første og sidste dag -- Mads Nielsen
	// A/S
	public double doegnDosis() {

		if (dosisGivetDatoer.size() == 0) {
			System.out.println("Returned");
			return 0;
		}
			

		// Få fat i den tidligste dato
		LocalDate min = dosisGivetDatoer.get(0);
		for (int i = 1; i < dosisGivetDatoer.size(); i++) {
			if (dosisGivetDatoer.get(i).isBefore(min)) {
				min = dosisGivetDatoer.get(i);
			}
		}

		// Få fat i den seneste dato
		LocalDate max = dosisGivetDatoer.get(0);
		for (int i = 1; i < dosisGivetDatoer.size(); i++) {
			if (dosisGivetDatoer.get(i).isAfter(max)) {
				max = dosisGivetDatoer.get(i);
			}
		}

		double sum = antalEnheder * dosisGivetDatoer.size();

		return sum / (ChronoUnit.DAYS.between(min, max) + 1);
	}

	public double samletDosis() {
		return antalEnheder * dosisGivetDatoer.size();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return dosisGivetDatoer.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
