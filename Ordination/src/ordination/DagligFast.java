package ordination;

import java.time.*;
import java.util.Arrays;

public class DagligFast extends Ordination {
	private Dosis[] dosiser;

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, 
			double antalMorgen, double antalMiddag, double antalAften, double antalNat) {
		super(startDen, slutDen, laegemiddel);
		dosiser = new Dosis[4];
		createDosiser(antalMorgen, antalMiddag, antalAften, antalNat);
	}

	public Dosis[] createDosiser(double antalMorgen, double antalMiddag, double antalAften, double antalNat) {
		if (antalMorgen > 0 )
			dosiser[0] = new Dosis(LocalTime.of(6, 0), antalMorgen);
		if (antalMiddag > 0)
			dosiser[1] = new Dosis(LocalTime.of(12, 0), antalMiddag);
		if (antalAften > 0)
			dosiser[2] = new Dosis(LocalTime.of(18, 0), antalAften);
		if (antalNat > 0)
			dosiser[3] = new Dosis(LocalTime.of(0, 0), antalNat);
		return dosiser;
	}
	
	@Override
	public double samletDosis() {	
		return super.antalDage() * doegnDosis();
	}

	@Override
	public double doegnDosis() {
		double antal = 0;
		for (Dosis dosis : dosiser) {
			if (dosis != null) {
				antal += dosis.getAntal();
			}
		}
		return antal;
	}

	@Override
	public String getType() {
		return "Daglig fast";
	}
	
	public Dosis[] getDoser() {
		return Arrays.copyOf(dosiser, dosiser.length);
	}
}