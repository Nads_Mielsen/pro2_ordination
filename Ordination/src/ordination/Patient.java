package ordination;

import java.util.ArrayList;

import javafx.scene.image.Image;

public class Patient {
    private String cprnr;
    private String navn;
    private double vaegt;
    private Image img;
    private ArrayList<Ordination> ordinationer;

    public Patient(String cprnr, String navn, double vaegt) {
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
        this.ordinationer = new ArrayList<>();
    }

    public String getCprnr() {
        return cprnr;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public double getVaegt(){
        return vaegt;
    }

    public void setVaegt(double vaegt){
        this.vaegt = vaegt;
    }

    public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}
    
	public void addOrdination(Ordination ordination) {
    	if(!this.ordinationer.contains(ordination)) {
    		this.ordinationer.add(ordination);
    	}
    }
    
    public ArrayList<Ordination> getOrdinationer() {
    	return new ArrayList<Ordination>(this.ordinationer);
    }

    @Override
    public String toString(){
        return navn + "  " + cprnr;
    }

}
