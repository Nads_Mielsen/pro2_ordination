package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    private ArrayList<Dosis> dosis;
    
    public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
    	super(startDen, slutDen, laegemiddel);
    	dosis = new ArrayList<Dosis>();
    }

    public void opretDosis(LocalTime tid, double antal) {
    	Dosis temp = new Dosis(tid, antal);
    	dosis.add(temp);
    }

	@Override
	public double samletDosis() {
		double sum = 0;
		for (Dosis d : dosis) {
			sum += d.getAntal();
		}
		return sum * this.antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : dosis) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Skaev";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(dosis);
	}
}
