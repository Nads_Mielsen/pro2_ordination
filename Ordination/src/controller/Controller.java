package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class Controller {
	private Storage storage;
	private static Controller controller;

	private Controller() {
		storage = new Storage();
	}

	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}

	public static Controller getTestController() {
		return new Controller();
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke Pre: startDen, slutDen, patient og laegemiddel er
	 * ikke null Pre: antal >= 0
	 * 
	 * @return opretter og returnerer en PN ordination.
	 */
	public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double antal) throws IllegalArgumentException {
		if (slutDen.isBefore(startDen)) {
			throw new IllegalArgumentException("Start dato kan ikke være efter slut dato");
		}
		PN temp = new PN(startDen, slutDen, laegemiddel, antal);
		patient.addOrdination(temp);
		return temp;
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null Pre: margenAntal,
	 * middagAntal, aftanAntal, natAntal >= 0
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double antalMorgen, double antalMiddag, double antalAften, double antalNat) {
		if (slutDen.isBefore(startDen)) {
			throw new IllegalArgumentException("Start dato kan ikke være efter slut dato");
		}
		DagligFast DF = new DagligFast(startDen, slutDen, laegemiddel, antalMorgen, antalMiddag, antalAften, antalNat);
		patient.addOrdination(DF);
		return DF;
	}

	/**
	 * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke.
	 * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige kastes
	 * også en IllegalArgumentException.
	 *
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null Pre: alle tal i
	 * antalEnheder > 0
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen,
			LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			LocalTime[] klokkeSlet, double[] antalEnheder)  {
		if (slutDen.isBefore(startDen)) {
			throw new IllegalArgumentException("Start dato kan ikke være efter slut dato");
		}
		DagligSkaev temp = new DagligSkaev(startDen, slutDen, laegemiddel);
		for (int i = 0; i < klokkeSlet.length; i++) {
			temp.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		patient.addOrdination(temp);
		return temp;
	}

	/**
	 * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis datoen
	 * ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException Pre: ordination og dato er ikke null
	 */
	public void ordinationPNAnvendt(PN ordination, LocalDate dato) {
		if (dato == null) {
			throw new IllegalArgumentException("Dato skal vælges");
		}
		else if (!ordination.givDosis(dato)) {
			throw new IllegalArgumentException("Dosis skal gives mellem start og slut dato");
		}
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages hensyn til
	 * patientens vægt). Det er en forskellig enheds faktor der skal anvendes, og
	 * den er afhængig af patientens vægt. Pre: patient og lægemiddel er ikke null
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double anbefaletDosis = 0;
		if (patient.getVaegt() < 25) {
			anbefaletDosis = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (patient.getVaegt() <= 125) {
			anbefaletDosis = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		} else {
			anbefaletDosis = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		}
		return anbefaletDosis;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af
	 * ordinationer. Pre: laegemiddel er ikke null
	 */
	public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut, Laegemiddel laegemiddel) {

		List<Patient> patienter = getAllPatienter();

		int count = 0;

		for (int i = 0; i < patienter.size(); i++) {
			double vægt = patienter.get(i).getVaegt();
			if (vægt >= vægtStart && vægt <= vægtSlut) {
				ArrayList<Ordination> ordinationer = patienter.get(i).getOrdinationer();
				for (Ordination ordination : ordinationer) {
					if (ordination.getLaegemiddel().equals(laegemiddel)) {
						count++;
					}
				}
			}
		}

		return count;
	}

	public List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}

	public Patient opretPatient(String cpr, String navn, double vaegt) {
		Patient p = new Patient(cpr, navn, vaegt);
		storage.addPatient(p);
		return p;
	}

	public Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
			double enhedPrKgPrDoegnTung, String enhed) {
		Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
				enhed);
		storage.addLaegemiddel(lm);
		return lm;
	}

	public void createSomeObjects() {
		this.opretPatient("121256-0512", "Jane Jensen", 63.4);
		Patient p7 = this.opretPatient("070985-1153", "Martin Wänglund", 83.2);
		Patient p6 = this.opretPatient("050972-1233", "Ulla Terkelsen", 89.4);
		Patient p5 = this.opretPatient("011064-1522", "Timm Vladimir", 59.9);
		Patient p4 = this.opretPatient("090149-2529", "Kim Larsen", 87.7);
		Patient p1 = this.opretPatient("010391-2529", "Mads 'Nads' Nielsen", 87.7);
		Patient p2 = this.opretPatient("030149-2529", "Kristoffer 'Stotter' Hermansen", 87.7);
		Patient p3 = this.opretPatient("090149-2529", "Jacob 'Barsøe' Barsøe", 87.7);
		Patient p8 = this.opretPatient("030685-1163", "Lego Martin", 0.053);

		this.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		this.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		this.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		this.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		this.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		this.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		this.opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		this.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		this.opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, 0, 1, 0);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		this.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
		
		
		this.opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				p1, storage.getAllLaegemidler().get(1), 2, 0, 1, 0);
		
		this.opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				p2, storage.getAllLaegemidler().get(1), 2, 0, 1, 0);
		
		this.opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				p3, storage.getAllLaegemidler().get(1), 2, 0, 1, 0);
		
		//Sæt profilbilleder og giv alle der ikke har ordinationer tilfældige skæve ordinationer
		p1.setImg(new Image("images/Mads.png"));
		this.givTilfældigeOrdinationer(p1);
		p2.setImg(new Image("images/Stotter.png"));
		this.givTilfældigeOrdinationer(p2);
		p3.setImg(new Image("images/Barsoe.png"));
		this.givTilfældigeOrdinationer(p3);
		p4.setImg(new Image("images/KimLarsen.png"));
		this.givTilfældigeOrdinationer(p4);
		p5.setImg(new Image("images/TimmVladimir.png"));
		p6.setImg(new Image("images/UllaTerkelsen.jpeg"));
		this.givTilfældigeOrdinationer(p6);
		p7.setImg(new Image("images/MartinWänglund.png"));
		this.givTilfældigeOrdinationer(p7);
		p8.setImg(new Image("images/LegoMartin.png"));
		this.givTilfældigeOrdinationer(p8);
	}

	//Se bort fra denne metode :)
	public void givTilfældigeOrdinationer(Patient p) {
		int ordinationer = (int)(Math.random() * 5);
		for (int i = 0; i < ordinationer; i++) {
			LocalDate start = LocalDate.of(1990 + (int)(Math.random() * 30), 1 + (int)(Math.random() * 10),1 + (int)(Math.random() * 25));
			LocalTime[] tid = new LocalTime[1];
			tid[0] = LocalTime.of((int)(Math.random() * 20), (int)(Math.random() * 10));
			double[] enheder = new double[1];
			enheder[0] = Math.random() * 5;
			this.opretDagligSkaevOrdination(start, start.plusDays((int)(Math.random() * 30)), p, storage.getAllLaegemidler().get(0), tid,enheder );
		}
	}
	
}
