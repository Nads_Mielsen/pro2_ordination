package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Dosis;

public class DagligFastTest {
	DagligFast o1;
	Laegemiddel l1;
	LocalDate d1;
	LocalDate d2;
	LocalDate d3;
	Dosis[] do1;
	
	@Before
    public void setUp() throws Exception {
		d1 = LocalDate.of(2021, 3, 8);
		d2 = LocalDate.of(2021, 3, 10);
		d3 = LocalDate.of(2021, 3, 9);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		o1 = new DagligFast(d1, d2, l1, 2, 0, 1, 0);
		do1 = new Dosis[4];
		do1 = o1.getDoser();
	}
	
	@Test
	public void testDagligFast() {
		// TC1
		DagligFast test1 = new DagligFast(d1, d2, l1, 0, 0, 0, 0);
		assertNotNull(test1);
		assertEquals(3, test1.antalDage());
		assertEquals(0, test1.doegnDosis(), 0.001);
		
		// TC2
		DagligFast test2 = new DagligFast(d1, d2, l1, 1, 0, 0, 0);
		assertNotNull(test2);
		assertEquals(3, test2.antalDage());
		assertEquals(1, test2.doegnDosis(), 0.001);
		
		// TC3
		DagligFast test3 = new DagligFast(d1, d2, l1, 1, 1, 1, 1);
		assertNotNull(test3);
		assertEquals(3, test3.antalDage());
		assertEquals(4, test3.doegnDosis(), 0.001);
		
		// TC4
		DagligFast test4 = new DagligFast(d1, d2, l1, 10000, 0, 0, 0);
		assertNotNull(test4);
		assertEquals(3, test4.antalDage());
		assertEquals(10000, test4.doegnDosis(), 0.001);
	}
	
	@Test
	public void testSamletDosis() {
		// TC5
		DagligFast test5 = new DagligFast(d2, d2, l1, 0, 0, 0, 0);
		assertEquals(1, test5.antalDage());
		assertEquals(0, test5.doegnDosis(), 0.0001);
		assertEquals(0, test5.samletDosis(), 0.0001);
		
		// TC6
		DagligFast test6 = new DagligFast(d2, d2, l1, 1, 0, 0, 0);
		assertEquals(1, test6.antalDage());
		assertEquals(1, test6.doegnDosis(), 0.0001);
		assertEquals(1, test6.samletDosis(), 0.0001);
				
		// TC7
		DagligFast test7 = new DagligFast(d1, d2, l1, 1, 0, 0, 0);
		assertEquals(3, test7.antalDage());
		assertEquals(1, test7.doegnDosis(), 0.0001);
		assertEquals(3, test7.samletDosis(), 0.0001);
		
		// TC8
		DagligFast test8 = new DagligFast(d2, d1, l1, 0, 0, 0, 0);
		assertEquals(-1, test8.antalDage());
		assertEquals(0, test8.doegnDosis(), 0.0001);
		assertEquals(0, test8.samletDosis(), 0.0001);
				
		// TC9
		DagligFast test9 = new DagligFast(d2, d3, l1, -1, 0, 0, 0);
		assertEquals(0, test9.antalDage());
		assertEquals(0, test9.doegnDosis(), 0.0001);
		assertEquals(0, test9.samletDosis(), 0.0001);
				
		// TC10
		DagligFast test10 = new DagligFast(d2, d1, l1, -1, 0, 0, 0);
		assertEquals(-1, test10.antalDage());
		assertEquals(0, test10.doegnDosis(), 0.0001);
		assertEquals(0, test10.samletDosis(), 0.0001);
	}
	
	@Test
	public void testDoegnDosis() {
		// TC11
		DagligFast test11 = new DagligFast(d1, d2, l1, 1, 1, 1, 1);
		assertEquals(4, test11.doegnDosis(), 0.0001);
		
		// TC12
		DagligFast test12 = new DagligFast(d1, d2, l1, 1, 1, 0, 0);
		assertEquals(2, test12.doegnDosis(), 0.0001);
				
		// TC13
		DagligFast test13 = new DagligFast(d1, d2, l1, 0, 0, 0, 0);
		assertEquals(0, test13.doegnDosis(), 0.0001);
				
		// TC14
		DagligFast test14 = new DagligFast(d1, d2, l1, 1000, 2000, 3000, 4000);
		assertEquals(10000, test14.doegnDosis(), 0.0001);
	}
}
