package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;

public class OrdinationTest {

	Ordination ordination;
	@Before
	public void setUp() throws Exception {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 15);
		ordination = new DagligSkaev(startDen, slutDen, null);
	}

	@Test
	public void TC73() {
		Laegemiddel laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		assertEquals(null, ordination.getLaegemiddel());
		ordination.setLaegemiddel(laegemiddel);
		assertEquals(laegemiddel, ordination.getLaegemiddel());
	}
	
	@Test
	public void TC74() {
		Laegemiddel laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		assertEquals(null, ordination.getLaegemiddel());
		ordination.setLaegemiddel(laegemiddel);
		assertEquals(laegemiddel, ordination.getLaegemiddel());
		ordination.setLaegemiddel(null);
		assertEquals(null, ordination.getLaegemiddel());
	}
}