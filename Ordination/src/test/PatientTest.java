package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PatientTest {

	@Test
	public void testAddOrdination() {
		
		Patient p = new Patient("221133-0132", "Mads Nielsen", 82);
		
		//TC 44
		PN pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), new Laegemiddel("testMiddel", 1, 2, 3,"ml"), 3);
		p.addOrdination(pn);
		assertEquals(true, p.getOrdinationer().contains(pn));
		
		//TC 45
		p.addOrdination(pn);
		assertEquals(1, p.getOrdinationer().size());
		
		//TC 46
		p.addOrdination(null);
		assertEquals(true, p.getOrdinationer().contains(null));
		
	}

}
