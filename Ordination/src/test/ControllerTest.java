package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import controller.Controller;

public class ControllerTest {

	private Laegemiddel lm;
	private Controller ctrl;
	
	@Before
    public void setUp() throws Exception {
    	lm = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
    	ctrl = Controller.getController();
    }
	
	@Test
	public void testOpretPNOrdination() {
		
		//TC 48
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		PN pn = ctrl.opretPNOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), p, lm, 0);
		assertEquals(true, p.getOrdinationer().contains(pn));
		
		//TC 49
		p = new Patient("221133-0524", "Mads Nielsen", 82);
		pn = ctrl.opretPNOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), p, lm, 6);
		assertEquals(6, pn.getAntalEnheder(),0.0);
		
		//TC 50
		p = new Patient("221133-0524", "Mads Nielsen", 82);
		pn = ctrl.opretPNOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 10), p, lm, 6);
		assertEquals(6, pn.getAntalEnheder(),0.0);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNOrdinationIllegalArgument() {
		//TC 51
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		ctrl.opretPNOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 9), p, lm, 1);
	}
	
	@Test
	public void testOpretDagligFastOrdination() {
		//TC 52
		Patient p = new Patient("121256-0512", "Jane Jensen", 63.4);
		DagligFast DF = ctrl.opretDagligFastOrdination(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), p, lm, 0, 0, 0, 0);
		assertEquals(true, p.getOrdinationer().contains(DF));
		assertEquals(0, DF.doegnDosis(),0.0);
		
		//TC 53
		p = new Patient("121256-0512", "Jane Jensen", 63.4);
		DF = ctrl.opretDagligFastOrdination(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), p, lm, 1, 0, 0, 0);
		assertEquals(1, DF.doegnDosis(), 0.0);

		// TC 54
		p = new Patient("121256-0512", "Jane Jensen", 63.4);
		DF = ctrl.opretDagligFastOrdination(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), p, lm, 1, 1, 1, 1);
		assertEquals(4, DF.doegnDosis(),0.0);
		
		//TC 55
		p = new Patient("121256-0512", "Jane Jensen", 63.4);
		DF = ctrl.opretDagligFastOrdination(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), p, lm, 10000, 0, 0, 0);
		assertEquals(10000, DF.doegnDosis(),0.0);	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void fejlOpretDagligFastOrdination() {
		//TC 56
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		ctrl.opretDagligFastOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 8), p, lm, 0, 0, 0, 0);
	}
	
	@Test(expected = NullPointerException.class)
	public void fejlOpretDagligFastOrdination2() {
		//TC 57
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		ctrl.opretDagligFastOrdination(null, null, p, lm, 1, 0, 0, 0);
	}
	
	@Test
	public void fejlOpretDagligFastOrdination3() {
		//TC 58
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		DagligFast DF = ctrl.opretDagligFastOrdination(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), p, lm, -1, -1, -1, -1);
		assertEquals(0, DF.doegnDosis(), 0.0);	
	}
	
	@Test
	public void testOpretDagligSkaevOrdination() {
		// TC59
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		LocalTime[] t = new LocalTime[3];
		t[0] = LocalTime.of(06, 30);
		t[1] = LocalTime.of(14, 00);
		t[2] = LocalTime.of(18, 30);
		double[] d = new double[3];
		d[0] = 2;
		d[1] = 2;
		d[2] = 2;
		DagligSkaev DS = ctrl.opretDagligSkaevOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), p, lm, t, d);
		assertEquals(true, p.getOrdinationer().contains(DS));
		
		// TC60
		p = new Patient("221133-0524", "Mads Nielsen", 82);
		t = new LocalTime[3];
		t[0] = LocalTime.of(07, 30);
		t[1] = LocalTime.of(13, 00);
		t[2] = LocalTime.of(14, 30);
		d = new double[3];
		d[0] = 5;
		d[1] = 5;
		d[2] = 5;
		DS = ctrl.opretDagligSkaevOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 19), p, lm, t, d);
		assertEquals(true, p.getOrdinationer().contains(DS));
	}
	
	@Test
	public void fejlOpretDagligSkaevOrdination() {
		// TC61
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		LocalTime[] t = new LocalTime[0];
		double[] d = new double[0];
		DagligSkaev DS = ctrl.opretDagligSkaevOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), p, lm, t, d);
		assertEquals(true, p.getOrdinationer().contains(DS));
		
		// TC62
		p = new Patient("221133-0524", "Mads Nielsen", 82);
		t = new LocalTime[3];
		t[0] = LocalTime.of(06, 30);
		t[1] = LocalTime.of(14, 00);
		t[2] = LocalTime.of(18, 30);
		d = new double[3];
		d[0] = -1;
		d[1] = -1;
		d[2] = -1;
		DS = ctrl.opretDagligSkaevOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), p, lm, t, d);
		assertEquals(true, p.getOrdinationer().contains(DS));
	}
	
	@Test
	public void testOrdinationPNAnvendt() {
		// TC63
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		PN pn = ctrl.opretPNOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), p, lm, 0);
		ctrl.ordinationPNAnvendt(pn, LocalDate.of(2021, 3, 12));
		assertEquals(true, pn.getDosisGivetDatoer().contains(LocalDate.of(2021, 3, 12)));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void fejlOrdinationPNAnvendt() {
		// TC64
		Patient p = new Patient("221133-0524", "Mads Nielsen", 82);
		PN pn = ctrl.opretPNOrdination(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), p, lm, 0);
		ctrl.ordinationPNAnvendt(pn, LocalDate.of(2021, 3, 16));
	}
	
	@Test 
	public void testAnbefaletDosisPrDoegn() {
		// TC65
		Patient p = new Patient("221133-0524", "Mads Nielsen", 22);
		assertEquals(2.2, ctrl.anbefaletDosisPrDoegn(p, lm), 0.0001);
		
		// TC66
		p = new Patient("221133-0524", "Mads Nielsen", 53);
		assertEquals(7.95, ctrl.anbefaletDosisPrDoegn(p, lm), 0.0001);
				
		// TC67
		p = new Patient("221133-0524", "Mads Nielsen", 150);
		assertEquals(24, ctrl.anbefaletDosisPrDoegn(p, lm), 0.0001);
	}
}
