package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {

	private Laegemiddel lm;
	
    @Before
    public void setUp() throws Exception {
    	lm = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
    }

	@Test
	public void testGivDosis() {
		//TC 15
		PN pn = new PN(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 5), lm , 2);
		assertEquals(true,pn.givDosis(LocalDate.of(2021, 3, 3)));
		
		//TC 16
		assertEquals(false, pn.givDosis(LocalDate.of(2021, 3, 7)));
	}
	
	@Test
	public void testDoegnDosis() {
		
		//TC 17
		PN pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, 0);
		pn.givDosis(LocalDate.of(2021, 3, 10));
		assertEquals(0,pn.doegnDosis(),0.0);
		
		//TC 18
		pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, 1);
		assertEquals(0,pn.doegnDosis(),0.0);
		
		//TC 19
		pn.givDosis(LocalDate.of(2021, 3, 10));
		assertEquals(1, pn.doegnDosis(),0.0);
		
		//TC 20
		pn.givDosis(LocalDate.of(2021, 3, 11));
		pn.givDosis(LocalDate.of(2021, 3, 12));
		pn.givDosis(LocalDate.of(2021, 3, 13));
		pn.givDosis(LocalDate.of(2021, 3, 14));
		pn.givDosis(LocalDate.of(2021, 3, 15));
		assertEquals(1,pn.doegnDosis(),0.0);
		
		//TC 21
		pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, 1);
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		assertEquals(6,pn.doegnDosis(),0.0);
		
		//TC 22
		pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, -1);
		assertEquals(0, pn.doegnDosis(),0.0);
		
		//TC 23
		pn.givDosis(LocalDate.of(2021, 3, 10));
		assertEquals(-1, pn.doegnDosis(),0.0);
		
		//TC 24
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		pn.givDosis(LocalDate.of(2021, 3, 10));
		assertEquals(-6,pn.doegnDosis(),0.0);
	}
	
	@Test
	public void testSamletDosis() {
		
		//TC 25
		PN pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, 0);
		assertEquals(0, pn.samletDosis(),0.0);
		
		//TC 26
		pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, 1);
		assertEquals(0, pn.samletDosis(),0.0);
		
		//TC 27
		pn.givDosis(LocalDate.of(2021, 3, 12));
		assertEquals(1, pn.samletDosis(),0.0);
		
		//TC 28
		pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, 0);
		pn.givDosis(LocalDate.of(2021, 3, 12));
		assertEquals(0, pn.samletDosis(),0.0);
		
		//TC 29
		pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, -1);
		pn.givDosis(LocalDate.of(2021, 3, 12));
		assertEquals(-1, pn.samletDosis(),0.0);
		
		//TC 30
		pn = new PN(LocalDate.of(2021, 3, 10), LocalDate.of(2021, 3, 15), lm, -6);
		pn.givDosis(LocalDate.of(2021, 3, 12));
		assertEquals(-6, pn.samletDosis(),0.0);
	}
}
