package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;

public class DagligSkaevTest {

	
	Laegemiddel laegemiddel;
	@Before
	public void setUp() throws Exception {
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk"); 
	}

	//opretDosis()
	@Test
	public void TC31() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 15);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		assertEquals(0, dagligskaev.getDoser().size());
		dagligskaev.opretDosis(LocalTime.of(12, 0), 12);
		assertEquals(1, dagligskaev.getDoser().size());
		assertEquals(12, dagligskaev.getDoser().get(0).getAntal(), 0);
		assertEquals(LocalTime.of(12, 0), dagligskaev.getDoser().get(0).getTid());
	}
	
	@Test
	public void TC32() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 15);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		assertEquals(0, dagligskaev.getDoser().size());
		dagligskaev.opretDosis(LocalTime.of(12, 0), 0);
		assertEquals(1, dagligskaev.getDoser().size());
		assertEquals(0, dagligskaev.getDoser().get(0).getAntal(), 0);
		assertEquals(LocalTime.of(12, 0), dagligskaev.getDoser().get(0).getTid());
	}
	
	@Test
	public void TC33() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 15);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		assertEquals(0, dagligskaev.getDoser().size());
		dagligskaev.opretDosis(LocalTime.of(12, 0), -5);
		assertEquals(1, dagligskaev.getDoser().size());
		assertEquals(-5, dagligskaev.getDoser().get(0).getAntal(), 0);
		assertEquals(LocalTime.of(12, 0), dagligskaev.getDoser().get(0).getTid());
	}
	
	//samletDosis()
	@Test
	public void TC34() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 10);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		assertEquals(0, dagligskaev.getDoser().size());
		assertEquals(0, dagligskaev.samletDosis(), 0);
	}
	
	@Test
	public void TC35() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 10);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), 100);
		assertEquals(1, dagligskaev.getDoser().size());
		assertEquals(100, dagligskaev.samletDosis(), 0);
	}
	
	@Test
	public void TC36() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 19);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), 1);
		assertEquals(1, dagligskaev.getDoser().size());
		assertEquals(10, dagligskaev.samletDosis(), 0);
	}
	
	//Ugylding
	@Test
	public void TC37() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 8);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		assertEquals(0, dagligskaev.getDoser().size());
		assertEquals(0, dagligskaev.samletDosis(), 0);
	}
	
	@Test
	public void TC38() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 9);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), -1);
		assertEquals(1, dagligskaev.getDoser().size());
		assertEquals(0, dagligskaev.samletDosis(), 0);
	}
	
	@Test
	public void TC39() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 8);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), -1);
		assertEquals(1, dagligskaev.getDoser().size());
		assertEquals(1, dagligskaev.samletDosis(), 0);
	}
	
	//doegnDosis()
	@Test
	public void TC40() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 9);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(13, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(15, 0), 1);
		assertEquals(4, dagligskaev.getDoser().size());
		assertEquals(4, dagligskaev.doegnDosis(), 0);
	}
	
	@Test
	public void TC41() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 9);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(13, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 0);
		dagligskaev.opretDosis(LocalTime.of(15, 0), 0);
		assertEquals(4, dagligskaev.getDoser().size());
		assertEquals(2, dagligskaev.doegnDosis(), 0);
	}
	
	@Test
	public void TC42() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 9);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), 0);
		dagligskaev.opretDosis(LocalTime.of(13, 0), 0);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 0);
		dagligskaev.opretDosis(LocalTime.of(15, 0), 0);
		assertEquals(4, dagligskaev.getDoser().size());
		assertEquals(0, dagligskaev.doegnDosis(), 0);
	}
	
	@Test
	public void TC43() {
		LocalDate startDen = LocalDate.of(2021, 3, 10);
		LocalDate slutDen = LocalDate.of(2021, 3, 9);
		DagligSkaev dagligskaev = new DagligSkaev(startDen, slutDen, laegemiddel);
		
		dagligskaev.opretDosis(LocalTime.of(12, 0), 1000);
		dagligskaev.opretDosis(LocalTime.of(13, 0), 2000);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 3000);
		dagligskaev.opretDosis(LocalTime.of(15, 0), 4000);
		assertEquals(4, dagligskaev.getDoser().size());
		assertEquals(10000, dagligskaev.doegnDosis(), 0);
	}
}
